# https://gitlab.com/dev65asia/gitlab-checkout

this action checks-out your repository under $CI_PROJECT_PATH as $CI_COMMIT_SHA, so your pipeline can access it. commits after_script.

variables:
- GL_USER_NAME
- GL_TOKEN
